package app;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.Type;
import org.hibernate.criterion.Restrictions;

import com.fasterxml.jackson.annotation.JsonBackReference;

@SuppressWarnings("serial")
@Entity
@Table(name="commande_has_elements")
public class CommandeElementAssoc implements Serializable
{
	@Id @GeneratedValue
	@Column(name = "id_c_has_e")
	private int id;
	
	@Column(name = "id_commande")
	private int id_commande;
	
	@Column(name = "id_element")
	private int id_element;
	
	@Column(name = "quantite")
	private int quantite;
	
	@Column(name = "etat")
	private String etat;
		
	@ManyToOne
	@JoinColumn(name = "id_commande", updatable = false, insertable = false, referencedColumnName = "id_commande")
	@JsonBackReference
	private Commande commande;
	
	@ManyToOne
	@JoinColumn(name = "id_element", updatable = false, insertable = false, referencedColumnName = "id_element")
	private Element element;
	
	public CommandeElementAssoc()
	{

	}
	
	public static List<CommandeElementAssoc> recupererDerniersElementsPrisEnCompte()
	{
		@SuppressWarnings("unchecked")
		List<CommandeElementAssoc> elements = (List<CommandeElementAssoc>) MonHibernate.creerSession().createCriteria(CommandeElementAssoc.class)
				.add(Restrictions.eq("etat", "PRIS EN COMPTE"))
				.list();
		
		return elements;
	}
	
	public static List<CommandeElementAssoc> recupererDerniersElementsPrets()
	{
		@SuppressWarnings("unchecked")
		List<CommandeElementAssoc> elements = (List<CommandeElementAssoc>) MonHibernate.creerSession().createCriteria(CommandeElementAssoc.class)
				.add(Restrictions.eq("etat", "PRET"))
				.list();
		
		return elements;
	}
	
	public void modifierEtatDunElement(String _etat)
	{
		Session session = MonHibernate.creerSession();
		Transaction transaction = null;
		
		try {
			transaction = session.beginTransaction();			
			this.setEtat(_etat);
			session.update(this);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public float getTotal()
	{
		return this.quantite * this.getElement().getPrix();
	}
	
	public String toString()
	{
		String rslt = "ASSOC:\n";
		rslt += this.commande + "\n";
		rslt += this.element;
		
		return rslt;
	}

	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public Commande getCommande() {
		return commande;
	}
	public void setCommande(Commande commande) {
		this.commande = commande;
		this.id_commande = commande.getId();
	}
	public Element getElement() {
		return element;
	}
	public void setElement(Element element) {
		this.element = element;
		this.id_element = element.getId();
	}
	public int getId_commande() {
		return id_commande;
	}
	public void setId_commande(int id_commande) {
		this.id_commande = id_commande;
	}
	public int getId_element() {
		return id_element;
	}
	public void setId_element(int id_element) {
		this.id_element = id_element;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
}
