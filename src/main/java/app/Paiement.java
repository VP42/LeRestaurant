package app;

import javax.persistence.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "paiements")
public class Paiement 
{
	@Id @GeneratedValue
	@Column(name = "id_paiement")
	private int id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_commande")
	@JsonBackReference
	private Commande commande;
	
	@Column(name = "montant")
	private float montant;
	
	@Column(name = "moyen")
	private String moyenDePaiement;

	public Paiement()
	{
		
	}
	
	public Paiement(int _id_commande, float _montant, String _moyenDePaiement)
	{
		Session session = MonHibernate.creerSession();
		Transaction transaction = null;
		Commande _commande = (Commande) session.get(Commande.class, new Integer(_id_commande));
		
		this.commande = _commande;
		this.montant = _montant;
		this.moyenDePaiement = _moyenDePaiement;
		
		if (this.commande.getTotalDu() - this.montant == 0.0) {
			this.commande.mettreAjourEtat("TERMINEE");
		}
		
		try {
			transaction = session.beginTransaction();
			session.save(this);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	public String getMoyenDePaiement() {
		return moyenDePaiement;
	}

	public void setMoyenDePaiement(String moyenDePaiement) {
		this.moyenDePaiement = moyenDePaiement;
	}

	public Commande getCommande() {
		return commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}
}
