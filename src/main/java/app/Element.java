package app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "elements")
public class Element 
{
	@Id @GeneratedValue
	@Column(name = "id_element")
	private int id;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "prix")
	private float prix;

	public String toString()
	{
		return this.nom + " EUR" + this.prix;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}
}
