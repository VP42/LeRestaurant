package app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class SrvltAdmin extends HttpServlet
{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{				
		req.setAttribute("toutesLesCommandes", Commande.listerToutes());		
		getServletContext().getRequestDispatcher("/WEB-INF/admin/index.jsp").forward(req, resp);
	}
}
