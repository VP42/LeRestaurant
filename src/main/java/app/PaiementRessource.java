package app;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("paiements")
public class PaiementRessource 
{
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Paiement addPaiement(
			@FormParam("idCommande") int idCommande,
			@FormParam("montant") double montant,
			@FormParam("moyen") String moyen)
	{
		return new Paiement(idCommande, (float) montant, moyen);
	}
}
