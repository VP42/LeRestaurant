package app;

import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.BatchSize;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

@Entity
@Table(name = "commandes")
public class Commande 
{
	@Id @GeneratedValue
	@Column(name = "id_commande")
	private int id;
	
	@Column(name = "numero_table")
	private int table;
	
	@Column(name = "etat")
	private String etat;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_client")
	private Client client;
	
	@OneToMany(mappedBy = "commande", fetch = FetchType.LAZY)
	@JsonManagedReference
	private List<CommandeElementAssoc> elements;
	
	@OneToMany(mappedBy = "commande", fetch = FetchType.LAZY)
	@JsonManagedReference
	private List<Paiement> paiements;
	
 	public Commande()
	{
		
	}
	
	public Commande(Client _client, int _table, String _etat)
	{
		this.client = _client;
		this.table = _table;
		this.etat = _etat;
		
		Session session = MonHibernate.creerSession();
		Transaction transaction = null;
		
		try {
			transaction = session.beginTransaction();
			session.save(this);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void mettreAjourEtat(String _etat)
	{
		Session session = MonHibernate.creerSession();
		Transaction transaction = null;
		
		try {
			transaction = session.beginTransaction();			
			this.setEtat(_etat);
			session.update(this);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Commande> listerToutes()
	{		
		return (List<Commande>) MonHibernate.creerSession().createCriteria(Commande.class)
				.addOrder(Order.desc("id"))
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Commande> listerEnAttenteDePaiement()
	{
		return (List<Commande>) MonHibernate.creerSession().createCriteria(Commande.class)
				.add(Restrictions.eq("etat", "PAIEMENT EN ATTENTE"))
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public static List<Commande> listerEnCours()
	{
		return (List<Commande>) MonHibernate.creerSession().createCriteria(Commande.class)
				.add(Restrictions.eq("etat", "EN COURS"))
				.list();
	}
	
	public void ajouterElement(Element _element, int _quantite)
	{
		Session session = MonHibernate.creerSession();
		Transaction transaction = null;
		
		CommandeElementAssoc elemExist = (CommandeElementAssoc) session.createCriteria(CommandeElementAssoc.class)
				.add(Restrictions.eq("id_commande", this.getId()))
				.add(Restrictions.eq("id_element", _element.getId()))
				.uniqueResult();
		
		if (elemExist != null) {
			elemExist.setQuantite(elemExist.getQuantite() + _quantite);
			
			try {
				transaction = session.beginTransaction();
				session.update(elemExist);
				transaction.commit();
			} catch (HibernateException e) {
				transaction.rollback();
				e.printStackTrace();
			}
			
		} else {
			CommandeElementAssoc association = new CommandeElementAssoc();
			association.setCommande(this);
			association.setElement(_element);
			association.setEtat("PRIS EN COMPTE");
			association.setQuantite(_quantite);
			this.elements.add(association);
			
			try {
				transaction = session.beginTransaction();
				session.save(association);
				transaction.commit();
			} catch (HibernateException e) {
				transaction.rollback();
				e.printStackTrace();
			}
		}
	}
	
	public double getTotal()
	{
		double total = 0.0;
		
		if (this.elements != null)
			for (CommandeElementAssoc cea : this.elements)
				total += cea.getTotal();
		
		return total;
	}
	
	public double getTotalDu()
	{
		double totalDu = this.getTotal();
		
		if (this.paiements != null)
			for (Paiement pmt : this.getPaiements())
				totalDu -= pmt.getMontant();
		
		return totalDu;
	}
	
	public String toString()
	{
		String chaine = "Commande #" + this.id + " : Table N°" + this.table + ", Client: " + this.client.getPrenom() + " " + this.client.getNom();
		for (CommandeElementAssoc elt : this.elements) {
			chaine += "\n    " + elt.getElement().getNom() + " EUR" + elt.getElement().getPrix() + " - " + elt.getEtat();
		}
		
		return chaine;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public int getTable() {
		return table;
	}
	public void setTable(int table) {
		this.table = table;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public List<CommandeElementAssoc> getElements() {
		return elements;
	}
	public void setElements(List<CommandeElementAssoc> elements) {
		this.elements = elements;
	}
	public List<Paiement> getPaiements() {
		return paiements;
	}
	public void setPaiements(List<Paiement> paiements) {
		this.paiements = paiements;
	}
}
