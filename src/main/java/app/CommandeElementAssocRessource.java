package app;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("elements-de-commande")
public class CommandeElementAssocRessource 
{
	/**
	 * POST /app/api/elements-de-commande/new
	 * @param idCommande
	 * @param idElement
	 * @return Commande
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Commande newElementDeCommande(
			@FormParam("idCommande") int idCommande,
			@FormParam("idElement") int idElement,
			@FormParam("quantite") int quantite)
	{
		Commande commandeActuelle = (Commande) MonHibernate.creerSession().get(Commande.class, new Integer(idCommande));
		Element elementAajouter = (Element) MonHibernate.creerSession().get(Element.class, new Integer(idElement));
		commandeActuelle.ajouterElement(elementAajouter, quantite);
		
		return commandeActuelle;
	}
	
	/**
	 * PUT /app/api/elements-de-commande
	 * @param idElementDeCommande
	 * @param etat
	 * @return
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public CommandeElementAssoc updateEtatDunElementDeCommande(
			@FormParam("idElement") int idElementDeCommande,
			@FormParam("etat") String etat)
	{
		CommandeElementAssoc cea = ((CommandeElementAssoc) MonHibernate.creerSession().get(CommandeElementAssoc.class, new Integer(idElementDeCommande)));
		cea.modifierEtatDunElement(etat);
		
		return cea;
	}
}
