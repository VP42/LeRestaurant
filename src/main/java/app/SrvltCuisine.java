package app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class SrvltCuisine extends HttpServlet
{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{				
		req.setAttribute("elementsPrisEnCompte", CommandeElementAssoc.recupererDerniersElementsPrisEnCompte());		
		getServletContext().getRequestDispatcher("/WEB-INF/admin/cuisine.jsp").forward(req, resp);
	}
}
