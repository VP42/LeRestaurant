package app;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;

@Path("clients")
public class ClientRessource 
{
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Client> tousLesClients() 
	{
		return (List<Client>) MonHibernate.creerSession().createCriteria(Client.class).list();
	}
	
	@GET
	@Path("/{idClient}")
	@Produces(MediaType.APPLICATION_JSON)
	public Client voirClient(@PathParam("idClient") int idClient) {
		return (Client) MonHibernate.creerSession().get(Client.class, new Integer(idClient));
	}
	
	@POST
	@Path("/check")
	@Produces(MediaType.APPLICATION_JSON)
	public Client verifierClient(
			@FormParam("email") String email,
			@FormParam("motDePasse") String motDePasse) throws NoSuchAlgorithmException, InvalidKeySpecException
	{	
		if (email == null || motDePasse == null)
			throw new WebApplicationException(404);
		
		Client client = (Client) MonHibernate.creerSession().createCriteria(Client.class)
				.add(Restrictions.eq("email", email))
				.uniqueResult();
				
		if (client != null) {
			if (PasswordHash.validatePassword(motDePasse, client.getMotDePasse()))
				return client;
			else
				throw new WebApplicationException(401);
		}
		
		throw new WebApplicationException(404);
	}
}
