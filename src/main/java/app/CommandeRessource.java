package app;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

@Path("commandes")
public class CommandeRessource 
{
	/**
	 * GET /app/api/commandes/get
	 * @return List<Commande>
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Commande> toutesLesCommandes() 
	{
		return (List<Commande>) MonHibernate.creerSession().createCriteria(Commande.class).list();
	}
	
	/**
	 * GET /app/api/commandes/get/IDCOMMANDE
	 * @param idCommande
	 * @return Commande
	 */
	@GET
	@Path("/{idCommande}")
	@Produces(MediaType.APPLICATION_JSON)
	public Commande getCommande (@PathParam("idCommande") int idCommande) 
	{
		return (Commande) MonHibernate.creerSession().get(Commande.class, new Integer(idCommande));
	}
	
	/**
	 * POST /app/api/commandes/new
	 * @param idClient
	 * @param table
	 * @return Commande
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Commande newCommande(
			@FormParam("idClient") int idClient,
			@FormParam("table") int table)
	{
		return new Commande(((Client) MonHibernate.creerSession().get(Client.class, new Integer(idClient))), table, "EN COURS");
	}
	
	/**
	 * POST /app/api/commandes/update
	 * @param _idCommande
	 * @param _etat
	 * @return Commande
	 */
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Commande updateEtat(
			@FormParam("idCommande") int _idCommande, 
			@FormParam("etat") String _etat) 
	{		
		Session sess = MonHibernate.creerSession();
		Commande commande = (Commande) sess.get(Commande.class, new Integer(_idCommande));
		MonHibernate.fermerSession(sess);
				
		commande.mettreAjourEtat(_etat);
		
		return commande;
	}
}
