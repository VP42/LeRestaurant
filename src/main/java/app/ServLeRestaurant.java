package app;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

@SuppressWarnings("serial")
public class ServLeRestaurant extends HttpServlet
{
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// CLIENT
		// ajouterClient();
		
		// COMMANDES
		// ajouterCommande();
		// mettreAjourCommande();
		// listerCommandes();
		// ajouterElement();
		// modifierEtatElement();
		// calculerTotal();
		// ajouterPaiement();
		test();
	}
	
	public void test()
	{
		System.out.println(CommandeElementAssoc.recupererDerniersElementsPrisEnCompte());
	}
	
	protected void ajouterPaiement() 
	{
		new Paiement(10, (float) 50.0, "ESPECES");
	}
	
	protected void calculerTotal()
	{
		System.out.println(((Commande) MonHibernate.creerSession().get(Commande.class, new Integer(10))).getTotal());
	}
	
	protected void modifierEtatElement()
	{
		((CommandeElementAssoc) MonHibernate.creerSession().get(CommandeElementAssoc.class, new Integer(9))).modifierEtatDunElement("A VOIR");
	}
	
	protected void ajouterCommande()
	{
		Client cl = (Client) MonHibernate.creerSession().get(Client.class, new Integer(2));
		new Commande(cl, 44, "TEST");
	}
	
	protected void mettreAjourCommande()
	{
		Session sess = MonHibernate.creerSession();
		Commande commande = (Commande) sess.get(Commande.class, new Integer(10));
		MonHibernate.fermerSession(sess);
		
		commande.mettreAjourEtat("OULALA");
	}
	
	protected void ajouterClient()
	{
		new Client("Wozniak", "Steve", "wozniak@apple.com", "0320424242");
	}
	
	protected void listerCommandes()
	{
		List<Commande> toutesLesCommandes = Commande.listerToutes();
		for (Commande cmd : toutesLesCommandes) {
			System.out.println(cmd);
		}
	}

	protected void ajouterElement()
	{
		Commande commandeActuelle = (Commande) MonHibernate.creerSession().get(Commande.class, new Integer(10));
		Element elementAajouter = (Element) MonHibernate.creerSession().get(Element.class, new Integer(5));
		
		commandeActuelle.ajouterElement(elementAajouter, 42);
	}
}
