package app;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class SrvltService extends HttpServlet
{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{				
		req.setAttribute("elementsEnAttenteDenvoi", CommandeElementAssoc.recupererDerniersElementsPrets());		
		getServletContext().getRequestDispatcher("/WEB-INF/admin/service.jsp").forward(req, resp);
	}
}
