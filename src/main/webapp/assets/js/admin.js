var idCommande;
var restantDu;

function afficherCommande(commande) {
	idCommande = commande.data('commande');
	restantDu = commande.data('restant_du');
	$.ajax({
		  url: "/app/api/commandes/"+idCommande,
		  data: {},
		  cache: false,
		  success: function(data) {
			  $('#pap_idCommande').text(data.id);
			  $('#pap_totalCommande').text(data.total.toFixed(2));
			  for (var i = 0; i < data.elements.length; i++) {
				  var etatsElement = '<select class="etatElement" data-id_element="'+data.elements[i].id+'"><option>PRIS EN COMPTE</option><option>EN PREPARATION</option><option>PRET</option><option>SERVI</option><option>ANNULE</option></select>';				
				  $('#pap_detailsElementsCommande > tbody:last').append('<tr><td>'+etatsElement+'</td><td>'+data.elements[i].quantite+'</td><td>'+data.elements[i].element.nom+'</td></td><td style=\"text-align: right;\">'+data.elements[i].element.prix.toFixed(2)+' EUR</td><td style=\"text-align: right;\">'+data.elements[i].total.toFixed(2)+' EUR</td></tr>');
				  $('#pap_detailsElementsCommande tr:last select option').each(function(j, v) {
					  if ($(v).val() == data.elements[i].etat)
						  $(v).attr('selected', 'selected');							  
				  });
			  }
			  $('#procederAuPaiementModal').modal();
		  },
		  dataType: "json"
	});
}

$(function() {
	
	document.cookie = "isAdmin=yes; ";
	
	$('.recapitulatifCommande').on('click', function() {
		afficherCommande($(this));
	});
	
	$(document).on('change', '.etatElement', function() {
		$.ajax({
			type: 'PUT',
			url: "/app/api/elements-de-commande",
			context: this,
			data: {idElement: $(this).data('id_element'), etat: ($(this).val())},
			success: function(data) {
				// console.log(data);
			},
			dataType: "json"
		});
	});
	
	$('#pap_payer').on('click', function() {
		// Remplissage des données pour l'encaissement
		$('#e_idCommande').text(idCommande);
		$('#e_montantRestantDu').text(restantDu);
		
		// Masquage du modal de récapitulatif
		$('#procederAuPaiementModal').modal('hide');
		
		// Affichage du modal d'encaissement
		$('#encaisserModal').modal();
	});
	
	$('#procederAuPaiementModal').on('hidden.bs.modal', function(e) {
		$('#pap_detailsElementsCommande > tbody').empty();
	});
	
	$('.payerCommande').on('click', function() {
		idCommande = $(this).data('commande');
		restantDu = $(this).data('restant_du');
		
		// Remplissage des données pour l'encaissement
		$('#e_idCommande').text(idCommande);
		$('#e_montantRestantDu').text(restantDu);
		
		// Affichage du modal d'encaissement
		$('#encaisserModal').modal();
	});
	
	$('#e_payer').on('click', function() {
		$.ajax({
			type: 'POST',
			url: '/app/api/paiements/',
			context: this,
			data: {
				idCommande: idCommande,
				montant: $('#e_montant').val(),
				moyen: $('#e_moyen').val()
			},
			success: function(data) {
				location.reload();
			},
			dataType: 'json'
		});
	});
});