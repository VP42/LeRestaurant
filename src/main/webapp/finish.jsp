<!DOCTYPE html><!--HTML5 doctype-->
<html>
<head>
	<title>Le Restaurant</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" />
	<link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
	<style type="text/css">
		*  { -webkit-user-select:none; -webkit-tap-highlight-color:rgba(255, 255, 255, 0); }
		input, textarea  { -webkit-user-select:text; }
		body { background-color:white; color:black }
		h2 { font-family: 'Damion', cursive; text-align: center; font-size: 42px; }
	</style>
	<script type="text/javascript">
		/* This code is used to run as soon as Intel activates */
		var onDeviceReady=function(){
		//hide splash screen
		intel.xdk.device.hideSplashScreen();
		};
		document.addEventListener("intel.xdk.device.ready",onDeviceReady,false);
	</script>
  <link rel="stylesheet" href="//code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css"/>
  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
  <script src="//code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
  <link rel="stylesheet" href="/app/assets/css/style.css" />
</head>
<body>
	<div data-role="header" data-theme="c"><h1>Le Restaurant</h1></div>
    <div data-role="content" data-theme="c" >
        <div class="ui-body ui-body-c ui-corner-all">
            <div class="ui-field-contain">
            	<h2>Le Restaurant</h2>
                <p style="text-align: center;"><strong>Un employ� va venir vous aider � r�gler votre commande dans quelques instants...</strong><br>
                Nous vous remercions de votre visite et vous souhaitons une excellente journ�e !</p>
            </div>
        </div>
    </div>
    <script src="/app/assets/js/script.js"></script>
</body>
</html>
