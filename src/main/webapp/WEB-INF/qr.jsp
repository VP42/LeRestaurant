<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css'>
	<style>
	body {
		background: url(/app/assets/img/cream_pixels.png);
	}
	#container {
		width: 600px;
		margin-left: auto;
		margin-right: auto;
		margin-top: 164px;
		text-align: center;
	}
	#container h1 {
		margin-bottom: 0;
		font-family: 'Damion', cursive;
		font-size: 52px;
	}
	#container p {
		font-family: 'Raleway', sans-serif;
		font-weight: 200;
	}
	</style>
</head>
<body>
	<div id="container">
		<img src="/app/assets/img/377.GIF" />
		<h1>Le Restaurant</h1>
		<p>
			Vous avez demmand� la Table N�<strong><% out.println(request.getParameter("table")); %></strong>, veuillez patienter...
		</p>
	</div>
</body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/app/assets/js/jquery.cookie.js"></script>
<script>

	if ($.cookie('isAdmin') == 'yes') {
		window.location.replace('/app/admin?table='+<% out.println(request.getParameter("table")); %>);
	} else {
		setTimeout(function() {
			document.cookie = "idTable" + "=" + <% out.println(request.getParameter("table")); %> +"; ";
			window.location.replace('/app/login.jsp');
		}, 3000);
	}
</script>
</html>
