<%@ page import="app.*" %>
<%@ page import="java.util.List" %>
<%@ include file="header.jsp" %>

<div class="page-header">
  <h1>Service</h1>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>El�ments en attente d'envoi</h2><br>
		<table class="table">
	      <thead>
	        <tr>
	          <th width="10%">Table</th>
	          <th width="10%">Quantit�</th>
	          <th width="60%">El�ment</th>
	          <th width="20%"></th>
	        </tr>
	      </thead>
	      <tbody>
			<%
			String labelEtat;	
			List<CommandeElementAssoc> elementsEnAttenteDenvoi = (List<CommandeElementAssoc>) request.getAttribute("elementsEnAttenteDenvoi");
				for (CommandeElementAssoc elem : elementsEnAttenteDenvoi) {
					out.print("<tr>");
						out.print("<td style='vertical-align: middle;'>");
							out.println(elem.getCommande().getTable());
						out.print("</td>");
						out.print("<td style='vertical-align: middle;'>");
							out.println(elem.getQuantite());
						out.print("</td>");
						out.print("<td style='vertical-align: middle;'>");
							out.println(elem.getElement().getNom());
						out.print("</td>");
						out.print("<td style='text-align: right;'><button class='btn btn-link'>Annuler</button><button class='btn btn-success elementServi' data-id_element='"+elem.getId()+"'>SERVI</button>");
					out.print("</tr>");
				}
			%>
	      </tbody>
	    </table>
	</div>
</div>

<script>
$(function() {
	$('.elementServi').on('click', function() {
		$.ajax({
	        type: 'PUT',
	        url: '/app/api/elements-de-commande/',
	        context: this,
	        data: { 
	            idElement: $(this).data('id_element'),
	            etat: 'SERVI',
	        },
	        success: function(data) {
	            location.reload(); 
	        }
	    });
	});
});
</script>

<%@ include file="footer.jsp" %>