<%@ page import="app.*" %>
<%@ page import="java.util.List" %>
<%@ include file="header.jsp" %>

<div class="page-header">
  <h1>Tableau de bord <small>Gestion du Restaurant</small></h1>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>Commandes en cours</h2><br>
		<table class="table">
	      <thead>
	        <tr>
	          <th width="5%">#</th>
	          <th width="10%">Table</th>
	          <th width="20%">Etat</th>
	          <th width="15%">Client</th>
	          <th style="text-align: right;" width="15%">Total</th>
	          <th style="text-align: right;" width="15%">Restant d�</th>
	          <th width="20%"></th>
	        </tr>
	      </thead>
	      <tbody>
			<%
			String labelEtat;	
			List<Commande> toutesLesCommandes = (List<Commande>) request.getAttribute("toutesLesCommandes");
				for (Commande cmd : toutesLesCommandes) {
					if (cmd.getEtat().equalsIgnoreCase("PAIEMENT EN ATTENTE")) {
						labelEtat = "danger";
					} else if (cmd.getEtat().equalsIgnoreCase("EN COURS")) {
						labelEtat = "warning";
					} else if (cmd.getEtat().equalsIgnoreCase("TERMINEE")) {
						labelEtat = "success";
					} else {
						labelEtat = "default";
					}
					out.print("<tr>");
						out.print("<td>");
							out.println(cmd.getId());
						out.print("</td>");
						out.print("<td>");
							out.println(cmd.getTable());
						out.print("</td>");
						out.print("<td>");
							out.println("<span class=\"label label-"+labelEtat+"\">"+cmd.getEtat()+"</span>");
						out.print("</td>");
						out.print("<td>");
							out.println(cmd.getClient().getNom().toUpperCase() + " " + cmd.getClient().getPrenom());
						out.print("</td>");
						out.print("<td style=\"text-align: right\">");
							out.println(cmd.getTotal() + " EUR");
						out.print("</td>");
						out.print("<td style=\"text-align: right\">");
							out.println(cmd.getTotalDu() + " EUR");
						out.print("</td>");
						out.print("<td style=\"text-align: right;\">");
						    out.print("<button type=\"button\" class=\"btn btn-default btn-xs btn-info recapitulatifCommande\" data-commande=\""+cmd.getId()+"\" data-table=\""+cmd.getTable()+"\" data-restant_du=\""+cmd.getTotalDu()+"\">R�capitulatif</button> &nbsp;");
							out.print("<button type=\"button\" class=\"btn btn-default btn-xs btn-danger payerCommande\" data-commande=\""+cmd.getId()+"\" data-restant_du=\""+cmd.getTotalDu()+"\">Payer</button>");
						out.print("</td");
					out.print("</tr>");
				}
			%>
	      </tbody>
	    </table>
	</div>
</div>

<br>
<div class="alert alert-success"><strong>Statut du serveur : </strong>OPERATIONNEL.</div>

<div id="procederAuPaiementModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Paiement de la commande #<span id="pap_idCommande"></span></h4>
      </div>
      <div class="modal-body">
        <table id="pap_detailsElementsCommande" class="table">
	      <thead>
	        <tr>
	          <th width="20%">Etat</th>
	          <th width="10%">Qt.</th>
	          <th width="35%">El�ment</th>
	          <th width="15%">P.U.</th>
	          <th width="20%">Total</th>
	        </tr>
	      </thead>
	      <tbody>
	      </tbody>
	    </table>
	    <p class="text-right text-danger" style="font-size: 20px">TOTAL : <strong><span id="pap_totalCommande"></span> EUR</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Fermer</button>
        <button type="button" id="pap_payer" class="btn btn-danger">Proc�der au paiement</button>
      </div>
    </div>
  </div>
</div>

<div id="encaisserModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Encaissement de la commande #<span id="e_idCommande"></span></h4>
      </div>
      <div class="modal-body">
		<form class="form-horizontal">
		  <fieldset>
		  <div class="form-group">
		    <label class="col-md-3 control-label" for="e_montant">Montant :</label>
		    <div class="col-md-8">
		      <div class="input-group">
		        <input id="e_montant" name="e_montant" class="form-control" placeholder="" type="number" required="">
		        <span class="input-group-addon">EUR</span>
		      </div>
		    </div>
		  </div>
		  <div class="form-group">
		    <label class="col-md-3 control-label" for="e_moyen">Moyen :</label>
		    <div class="col-md-8">
		      <select id="e_moyen" name="e_moyen" class="form-control">
		        <option value="RENDU">Rendu</option>
		        <option value="ESPECES">Esp�ces</option>
		        <option value="CHEQUE">Ch�que</option>
		        <option value="CB">Carte Bancaire</option>
		        <option value="TICKET RESTO">Ticket Restaurant</option>
		        <option value="AVOIR">Avoir</option>
		      </select>
		    </div>
		  </div>
		  </fieldset>
		</form>
		<h4 class="text-danger"> Montant restant d� : <strong><span id="e_montantRestantDu"></span> EUR</strong></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Fermer</button>
        <button type="button" id="e_payer" class="btn btn-danger">Payer</button>
      </div>
    </div>
  </div>
</div>

<script src="/app/assets/js/admin.js"></script>
<% if (request.getParameter("table") != null) { %>
<script>
	afficherCommande($('.recapitulatifCommande[data-table="<% out.print(request.getParameter("table")); %>"]'));
</script>
<% } %>

<%@ include file="footer.jsp" %>