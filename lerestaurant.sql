# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: 127.0.0.1 (MySQL 5.6.17)
# Base de données: lerestaurant
# Temps de génération: 2014-05-12 14:11:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id_client` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) DEFAULT NULL,
  `prenom` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mot_de_passe` varchar(120) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;

INSERT INTO `clients` (`id_client`, `nom`, `prenom`, `email`, `mot_de_passe`, `telephone`)
VALUES
	(1,'Jobs','Steve','steve@apple.com','1000:57305d1013f6f3dce70dfb4443d0c839997012690c6e6658:83b4e4fb4373a9b3a35dc5a350f760111555df1645986b05','0320424242'),
	(2,'Wozniak','Steve','wozniak@apple.com','1000:da8740f374bd4a05e6b60bdca75e74041723c9839ff8dc71:f575d2b50c556a3e027931014d884fd1b3c79c9f8d7d1b69','0320424242'),
	(3,'Page','Larry','page@google.com','1000:9aa72f7f977d94cc52b9647d54010f228c7ba9b55d93473c:2a6c42530a18fd3a696e845a11f6b8f40a9b973d3de7cbb7','0642424242'),
	(4,'Gates','Bill','gates@microsoft.com','1000:43a18fd64b864572f1636f85fc1f61867d68192bb31878e6:d4d7c03669108b04b5e11c086562880be98d43cdf65c0050','0624242424');

/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table commande_has_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `commande_has_elements`;

CREATE TABLE `commande_has_elements` (
  `id_c_has_e` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_commande` int(11) unsigned DEFAULT NULL,
  `id_element` int(11) unsigned DEFAULT NULL,
  `quantite` tinyint(3) unsigned DEFAULT '1',
  `etat` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_c_has_e`),
  KEY `id_commande` (`id_commande`),
  KEY `id_element` (`id_element`),
  CONSTRAINT `commande_has_elements_ibfk_1` FOREIGN KEY (`id_commande`) REFERENCES `commandes` (`id_commande`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `commande_has_elements_ibfk_2` FOREIGN KEY (`id_element`) REFERENCES `elements` (`id_element`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

LOCK TABLES `commande_has_elements` WRITE;
/*!40000 ALTER TABLE `commande_has_elements` DISABLE KEYS */;

INSERT INTO `commande_has_elements` (`id_c_has_e`, `id_commande`, `id_element`, `quantite`, `etat`)
VALUES
	(13,24,4,1,'SERVI'),
	(14,24,5,1,'SERVI');

/*!40000 ALTER TABLE `commande_has_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table commandes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `commandes`;

CREATE TABLE `commandes` (
  `id_commande` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(11) unsigned NOT NULL,
  `numero_table` int(11) NOT NULL,
  `etat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `id_client` (`id_client`),
  CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

LOCK TABLES `commandes` WRITE;
/*!40000 ALTER TABLE `commandes` DISABLE KEYS */;

INSERT INTO `commandes` (`id_commande`, `id_client`, `numero_table`, `etat`)
VALUES
	(24,1,42,'EN COURS');

/*!40000 ALTER TABLE `commandes` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `elements`;

CREATE TABLE `elements` (
  `id_element` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(200) DEFAULT NULL,
  `nom` varchar(250) DEFAULT NULL,
  `prix` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id_element`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;

INSERT INTO `elements` (`id_element`, `type`, `nom`, `prix`)
VALUES
	(4,'BOISSON','Coca-Cola Zero',2.50),
	(5,'BOISSON','Evian 0.5L',2.00),
	(6,'BOISSON','Evian 1L',3.50),
	(7,'BOISSON','Coca-Cola',2.50),
	(8,'BOISSON','Orangina',2.50),
	(9,'BOISSON','Diabolo',2.50),
	(10,'BOISSON','Perrier 0.5L',2.50),
	(14,'BOISSON','Verre de Vin',3.00),
	(15,'BOISSON','Verre de Biere ',3.00),
	(16,'DESSERT','Sorbet Ananas',3.50),
	(17,'DESSERT','Gateau au Chocolat',4.00),
	(18,'DESSERT','Gaufre Liegeoise',3.50),
	(19,'DESSERT','Creme Brulee',3.00),
	(20,'PLAT','Steak Frites',8.50),
	(21,'PLAT','Lasagnes',8.00),
	(22,'PLAT','Salade Cesar',7.50),
	(23,'PLAT','Bolognaises',8.50);

/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table paiements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paiements`;

CREATE TABLE `paiements` (
  `id_paiement` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_commande` int(11) unsigned NOT NULL,
  `montant` decimal(5,2) NOT NULL,
  `moyen` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_paiement`),
  KEY `id_commande` (`id_commande`),
  CONSTRAINT `paiements_ibfk_1` FOREIGN KEY (`id_commande`) REFERENCES `commandes` (`id_commande`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
